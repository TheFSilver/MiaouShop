# Miaoushop

## Team de choc :
Bastien, François, Anisaora, Marion

## Le shop
T'aimes les chats ? Mais ta meuf en veut pas ? Viens choisir ton nouveau fond d'écran pour le boulot, ton telephone et autres appareils en tout genre !

## Comment accéder au programme sur Heroku ?
Lien -> [Miaoushop](https://miaoushop.herokuapp.com/)<br />


## Comment accéder au programme en local ?
1. Se positionner sur le dossier avec le terminal
2. Lancer $ bundle install
3. Lancer $ rails db:migrate
4. Lancer $ rails db:seed
5. Lancer $ rails server
6. Se rendre sur localhost:3000 avec un navigateur
7. Et achete !
