Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  devise_for :users
  root 'items#index'
  resources :items, param: :slug
  resources :charges, :cartcontents, :orders
  post "add", to: "cartcontents#add"
  post "remove", to: "cartcontents#remove"
  get '/mail', to: 'items#mail'
end
