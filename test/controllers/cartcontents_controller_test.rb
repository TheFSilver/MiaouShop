require 'test_helper'

class CartcontentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @cartcontent = cartcontents(:one)
  end

  test "should get index" do
    get cartcontents_url
    assert_response :success
  end

  test "should get new" do
    get new_cartcontent_url
    assert_response :success
  end

  test "should create cartcontent" do
    assert_difference('Cartcontent.count') do
      post cartcontents_url, params: { cartcontent: {  } }
    end

    assert_redirected_to cartcontent_url(Cartcontent.last)
  end

  test "should show cartcontent" do
    get cartcontent_url(@cartcontent)
    assert_response :success
  end

  test "should get edit" do
    get edit_cartcontent_url(@cartcontent)
    assert_response :success
  end

  test "should update cartcontent" do
    patch cartcontent_url(@cartcontent), params: { cartcontent: {  } }
    assert_redirected_to cartcontent_url(@cartcontent)
  end

  test "should destroy cartcontent" do
    assert_difference('Cartcontent.count', -1) do
      delete cartcontent_url(@cartcontent)
    end

    assert_redirected_to cartcontents_url
  end
end
