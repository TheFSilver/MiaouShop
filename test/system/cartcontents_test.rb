require "application_system_test_case"

class CartcontentsTest < ApplicationSystemTestCase
  setup do
    @cartcontent = cartcontents(:one)
  end

  test "visiting the index" do
    visit cartcontents_url
    assert_selector "h1", text: "Cartcontents"
  end

  test "creating a Cartcontent" do
    visit cartcontents_url
    click_on "New Cartcontent"

    click_on "Create Cartcontent"

    assert_text "Cartcontent was successfully created"
    click_on "Back"
  end

  test "updating a Cartcontent" do
    visit cartcontents_url
    click_on "Edit", match: :first

    click_on "Update Cartcontent"

    assert_text "Cartcontent was successfully updated"
    click_on "Back"
  end

  test "destroying a Cartcontent" do
    visit cartcontents_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Cartcontent was successfully destroyed"
  end
end
