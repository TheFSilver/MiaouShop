require 'test_helper'

class UserTest < ActiveSupport::TestCase

  test 'valid user' do
    @user = User.new(email: "noreply@testme.com", firstname:"Frank", lastname:"Silver", admin: false, password:"Onsenfout7")
    assert @user.valid?
  end

  test 'invalid without email' do
    @user = User.new(firstname:"Frank", lastname:"Silver", admin: false, password:"Onsenfout7")
    assert_not @user.valid?
  end

  test 'invalid without password' do
    @user = User.new(email: "noreply@testme.com", firstname:"Frank", lastname:"Silver", admin: false)
    assert_not @user.valid?
  end

end
