require 'test_helper'

class ItemTest < ActiveSupport::TestCase

  test "should be valid" do
    @item = Item.new(title: "Test Title", description: "some description random text", image_url: "cat21.jpg", price: 19.99)
    assert @item.valid?
  end

  test "invalid without title" do
    @item = Item.new(description: "some description random text", image_url: "cat21.jpg", price: 19.99)
    assert_not @item.valid?
  end

  test "invalid without description" do
    @item = Item.new(title: "Test Title", image_url: "cat21.jpg", price: 19.99)
    assert_not @item.valid?
  end

  test "invalid without image_url" do
    @item = Item.new(title: "Test Title", description: "some description random text", price: 19.99)
    assert_not @item.valid?
  end

  test "invalid without price" do
    @item = Item.new(title: "Test Title", description: "some description random text", image_url: "cat21.jpg")
    assert_not @item.valid?
  end

end
