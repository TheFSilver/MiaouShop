require 'test_helper'

class CategoryTest < ActiveSupport::TestCase

  test "should be valid" do
    @category = Category.new(name: "Test name")
    assert @category.valid?
  end

  test "invalid without name" do
    @category = Category.new
    assert_not @category.valid?
  end

end
