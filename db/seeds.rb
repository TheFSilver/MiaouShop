# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
require 'faker'

# Creation de 20 items
20.times do |i|
    Item.create(title: Faker::Cat.breed, description: Faker::Lorem.sentence, price: 19.99 , image_url: "cat#{i+1}.jpg")
end

# Creation des catégories
Category.create(name: "Chat moche")
Category.create(name: "Chat méchant")
Category.create(name: "Chat bébé")
Category.create(name: "Chat cool")
Category.create(name: "Chat con")
Itemcat.create(item_id: 2, category_id: 1)
Itemcat.create(item_id: 10, category_id: 1)
Itemcat.create(item_id: 14, category_id: 1)
Itemcat.create(item_id: 17, category_id: 1)
Itemcat.create(item_id: 3, category_id: 2)
Itemcat.create(item_id: 8, category_id: 2)
Itemcat.create(item_id: 11, category_id: 2)
Itemcat.create(item_id: 14, category_id: 2)
Itemcat.create(item_id: 20, category_id: 2)
Itemcat.create(item_id: 4, category_id: 3)
Itemcat.create(item_id: 12, category_id: 3)
Itemcat.create(item_id: 15, category_id: 3)
Itemcat.create(item_id: 2, category_id: 4)
Itemcat.create(item_id: 7, category_id: 4)
Itemcat.create(item_id: 8, category_id: 4)
Itemcat.create(item_id: 9, category_id: 4)
Itemcat.create(item_id: 19, category_id: 4)
Itemcat.create(item_id: 6, category_id: 5)
Itemcat.create(item_id: 14, category_id: 5)

# Admin user for dashboard restricted access
user = User.create(email: "admin@miaoushop.com", firstname:"Admin", lastname:"Root", admin: true, password:"Devineca42")
