class CreateCartcontents < ActiveRecord::Migration[5.2]
  def change
    create_table :cartcontents do |t|
      t.belongs_to :user
      t.belongs_to :item
      t.integer :quantity, default: 1
      t.timestamps
    end
  end
end
