class CreateCartorders < ActiveRecord::Migration[5.2]
  def change
    create_table :cartorders do |t|
      t.belongs_to :order
      t.string :title
      t.text :description
      t.decimal :price
      t.string :image_url
      t.integer :quantity
      t.timestamps
    end
  end
end