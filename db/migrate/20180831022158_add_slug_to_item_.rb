class AddSlugToItem < ActiveRecord::Migration[5.2]
  def change
    add_column :items, :slug, :string
  end
end
