class CreateItemcats < ActiveRecord::Migration[5.2]
  def change
    create_table :itemcats do |t|
      t.belongs_to :item
      t.belongs_to :category
      t.timestamps
    end
  end
end