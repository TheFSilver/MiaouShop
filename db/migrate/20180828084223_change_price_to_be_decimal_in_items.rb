class ChangePriceToBeDecimalInItems < ActiveRecord::Migration[5.2]
  def change
    remove_column :items, :price
    add_column :items, :price, :decimal, :precision => 6, :scale => 2
  end
end
