json.extract! cartcontent, :id, :created_at, :updated_at
json.url cartcontent_url(cartcontent, format: :json)
