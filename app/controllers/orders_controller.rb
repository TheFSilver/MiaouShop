class OrdersController < ApplicationController
  before_action :correct_user,   only: [:show]

    def create
        Order.create(user_id: params[:format])

        @orderitems = Cartcontent.where(user_id: params[:format])
        @orderitems.each do |orderitem|
            item = Item.find(orderitem.item_id)
            Cartorder.create(order_id: Order.last.id, title: item.title, description: item.description, price: item.price, image_url: item.image_url, quantity: orderitem.quantity)
            orderitem.destroy
        end

        puts "..................................................."
        puts "Insérer ici stripe et mailer"
        puts "..................................................."

          # Amount in cents
          @amount = 1999

          customer = Stripe::Customer.create(
              :email => params[:stripeEmail],
              :source  => params[:stripeToken]
              )

          charge = Stripe::Charge.create(
              :customer    => customer.id,
              :amount      => @amount,
              :description => 'Rails Stripe customer',
              :currency    => 'eur'
              )

          flash[:notice] = "Commande passée ! Merci !"
          
          PaiementMailer.paiement_email(@amount, customer.email).deliver_later
          redirect_to order_path(current_user)

      rescue Stripe::CardError => e
          flash[:error] = e.message
          redirect_to new_charge_path
      end

    def show
        @orders = Order.where(user_id: params[:id])
    end

    private

    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless @user == current_user
    end
end
