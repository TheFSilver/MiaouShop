class ItemsController < ApplicationController
  def index
    if params[:format] == nil
      @current_category = "Tous"
      @item = Item.all
    else
      @current_category = Category.find(params[:format]).name
      @item = []
      Itemcat.where(category_id: params[:format]).each do |item|
        @item << Item.find(item.item_id)
      end
    end
  end

  def show
    @item = Item.find_by_slug(params[:slug])
  end
end
