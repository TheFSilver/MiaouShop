class CartcontentsController < ApplicationController
  before_action :correct_user,   only: [:show]

  def create
    if current_user == nil
      flash[:notice] = "Il faut te logger coco"

      redirect_to "/"
    else

      @currentitem = Cartcontent.where(user_id: current_user.id).where(item_id: params[:item_id])[0]

      if @currentitem != nil
        @currentitem.update(quantity: (@currentitem.quantity + 1))
      else
        Cartcontent.create(user_id: current_user.id, item_id: params[:item_id])
      end
      flash[:notice] = "Elément ajouté au panier"

      redirect_to "/"
    end
  end

  def add
    @currentitem = Cartcontent.find(params[:format])
    @currentitem.update(quantity: (@currentitem.quantity + 1))
    redirect_to request.referrer
  end

  def remove
    @currentitem = Cartcontent.find(params[:format])
    @currentitem.update(quantity: (@currentitem.quantity - 1))
    if @currentitem.quantity == 0
      @currentitem.destroy
    end
    redirect_to request.referrer
  end

  def index
    @cartcontents = Cartcontent.all
  end

  def show
    @cartcontent = Cartcontent.where(user_id: params[:id]).order(:item_id)

    @sum = 0
    @cartcontent.each do |cartcontent|
      @sum = ( Item.find(cartcontent.item_id).price * cartcontent.quantity ) + @sum
    end
  end

  def destroy
    Cartcontent.destroy(params[:id])
    redirect_to request.referrer
  end

  private

  def correct_user
    if !user_signed_in?
      redirect_to root_url
    else
      redirect_to root_url unless Cartcontent.where(user_id: current_user.id) == Cartcontent.where(user_id: params[:id])
    end
  end

end
