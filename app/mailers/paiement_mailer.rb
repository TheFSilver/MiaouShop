class PaiementMailer < ApplicationMailer
  def paiement_email(price, recipient)
      mail(from: "noreply@miaoushop.com", to: recipient,
           subject: "Order confirmation")
    end
end
