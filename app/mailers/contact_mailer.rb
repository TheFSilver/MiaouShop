class ContactMailer < ApplicationMailer
	default from: 'contact@test.fr'

	def contact
		mail(to: 'contact@test.fr', subject: 'Bienvenue')
	end
end
