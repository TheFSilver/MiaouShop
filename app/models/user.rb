class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,
         :trackable
  has_many :orders
  has_many :cartcontents
  has_many :items, through: :cartcontents

#  after_create :create_cart
#
#  def create_cart
#    @user = User.last
#    @cart = Cartcontent.create(user_id: @user.id)
#  end
end
