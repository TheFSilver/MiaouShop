class Category < ApplicationRecord
  has_many :itemcats
  has_many :items, through: :itemcats
  validates :name, presence: true
end
