class Item < ApplicationRecord
  has_many :cartcontents
  has_many :users, through: :cartcontents
  has_many :itemcats
  has_many :categories, through: :itemcats

  def self.add_slugs
    update(slug: to_slug(name))
  end

  extend FriendlyId
  friendly_id :title, use: :slugged

  validates :title, :description, :price, :image_url, presence: true

  
end
